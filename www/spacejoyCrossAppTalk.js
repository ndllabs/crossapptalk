var exec = require('cordova/exec')

exports.sayHello = function (arg0, success, error) {
  exec(success, error, 'spacejoyCrossAppTalk', 'sayHello', [arg0])
}

exports.launchUnity = function (
  param,
  onReturnedToIonicCallback,
  onMessageReceivedCallback
) {
  if (!param) {
    param = ''
  }

  exec(
    function (unityParam) {
      console.log('[[[unityParam]]] `: ', unityParam)
      if (unityParam) {
        if (unityParam.startsWith('RETURN')) {
          if (unityParam.length > 6) {
            onReturnedToIonicCallback(unityParam.substring(6))
          } else {
            onReturnedToIonicCallback('')
          }
        } else if (unityParam.startsWith('MESSAGE')) {
          if (unityParam.length > 7) {
            onMessageReceivedCallback(unityParam.substring(7))
          } else {
            onReturnedToIonicCallback('')
          }
        } else {
          console.log('ERROR: type of unityParam is not recognized!')
        }
      } else {
        console.log(
          'ERROR: parameter unityParam is null in Unity2Ionic callback!'
        )
      }
    },
    null,
    'spacejoyCrossAppTalk',
    'launchUnity',
    [param]
  )
}

exports.sendMessage = function (param) {
  if (!param) param = ''

  exec(null, null, 'spacejoyCrossAppTalk', 'sendMessage', [param])
}
