// Abishek Deb (vikz91.deb@gmail.com)
import { Injectable, NgZone } from '@angular/core';

import { Subject, Observable } from 'rxjs';
declare var cordova: any;
@Injectable({
  providedIn: 'root'
})
export class CrossapptalkService {
  private eventHandlerReturn: Subject<string>;
  private eventHandlerUnityMsg: Subject<string>;

  public EventHandlerReturn: Observable<string>;
  public EventHandlerUnityMsg: Observable<string>;

  constructor(private ngZone: NgZone) {
    this.eventHandlerReturn = new Subject<string>();
    this.eventHandlerUnityMsg = new Subject<string>();

    this.EventHandlerReturn = this.eventHandlerReturn.asObservable();
    this.EventHandlerUnityMsg = this.eventHandlerUnityMsg.asObservable();
  }

  public OpenUnity(data: string) {
    if (!cordova) {
      console.log(
        `|CORDOVA PLUGIN MOCK| [CrossAppTalk] [launchUnity]: ${data} `
      );
      return;
    }
    cordova.plugins.spacejoyCrossAppTalk.launchUnity(
      data,
      unityReturnRes => {
        this.ngZone.run(() => {
          this.eventHandlerReturn.next(unityReturnRes);
        });
      },
      msgRcvdRes => {
        this.ngZone.run(() => {
          this.eventHandlerUnityMsg.next(msgRcvdRes);
        });
      }
    );
  }

  public SendMessage(data: string) {
    if (!cordova) {
      console.log(
        `|CORDOVA PLUGIN MOCK| [CrossAppTalk] [sendMessage]: ${data} `
      );
      return;
    }
    cordova.plugins.spacejoyCrossAppTalk.sendMessage(data);
  }
}
