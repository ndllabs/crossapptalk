﻿using System.Runtime.InteropServices;
using UnityEngine;
using _unity;
using _unity.Events;

public class NativeAPI
{
    [DllImport("__Internal")]
    public static extern void showHostMainWindow(string lastStringColor);

    [DllImport("__Internal")]
    public static extern void sendMessageToIonic(string message);
}

public class CrossAppTalk : Singleton<CrossAppTalk>
{
    public void Init()
    {
        if (gameObject != null)
        {
            gameObject.name = "CrossAppTalk";
        }
        _.l("CrossAppTalk Enabled...");
    }

    private void OnEnable()
    {
        // Messenger<string>.AddListener("UnityCallbackEvent", OnMsgRcv);
        Messenger<string>.AddListener("Unity2IonicMsg", SendMessageToIonic);
    }

    private void OnDisable()
    {
        // Messenger<string>.RemoveListener("UnityCallbackEvent", OnMsgRcv);
        Messenger<string>.RemoveListener("Unity2IonicMsg", SendMessageToIonic);
    }

    void OnMsgRcv(string str)
    {
        Debug.Log("!!!!! INtercepting Event : " + str);
    }


    void UnityCallbackEvent(string args)
    {
        Messenger<string>.Broadcast("UnityCallbackEvent", args);
    }



    public void OpenIonic(string returnVal)
    {
#if UNITY_ANDROID
        // try
        // {
        //     AndroidJavaClass jc = new AndroidJavaClass("com.company.product.OverrideUnityActivity");
        //     AndroidJavaObject overrideActivity = jc.GetStatic<AndroidJavaObject>("instance");
        //     overrideActivity.Call("showMainActivity", lastStringColor);
        // } catch(Exception e)
        // {
        //     appendToText("Exception during showHostMainWindow");
        //     appendToText(e.Message);
        // }
#elif UNITY_IOS
        NativeAPI.showHostMainWindow(returnVal);
#endif
    }

    public void SendMessageToIonic(string msg)
    {
#if UNITY_ANDROID
    // @TODO
#elif UNITY_IOS
        NativeAPI.sendMessageToIonic(msg);
#endif
    }

}

