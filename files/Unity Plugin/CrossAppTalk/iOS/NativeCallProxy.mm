#import <Foundation/Foundation.h>
#import "NativeCallProxy.h"


@implementation FrameworkLibAPI

id<NativeCallsProtocol> api = NULL;
+(void) registerAPIforNativeCalls:(id<NativeCallsProtocol>) aApi
{
    api = aApi;
}

@end


extern "C" {
    void showHostMainWindow(const char* message) { return [api showHostMainWindow:[NSString stringWithUTF8String:message]]; }

    void sendMessageToIonic(const char* message) { return [api sendMessageToIonic:[NSString stringWithUTF8String:message]]; }
}

