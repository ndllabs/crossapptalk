# Author: Abhishek Deb (vikz91.deb@gmail.com)
# Run this everytime you add/remove plugins

#!/bin/bash
PATH_SRC=$(pwd)
PLATFORM=$1
buildName="Spacejoy Beta"
PATH_BUILD="$PATH_SRC/platforms/$PLATFORM"
REPLACE_DIR="$PATH_SRC/plugins/cordova-plugin-spacejoycrossapptalk/files/Ionic Xcode"
PATH_APP_DEL="$PATH_BUILD/$buildName/Classes"

# echo $PATH_SRC
# echo $PLATFORM
# echo $PATH_BUILD

echo "Welcome to Debamswamy's crossapp ionic unity setup!"

echo "Building Ionic ..."

npm i
ionic cordova build $PLATFORM

echo "Updating XCODE Project Files ..."
cp -f "$REPLACE_DIR/AppDelegate.h" "$PATH_APP_DEL"
cp -f "$REPLACE_DIR/AppDelegate.m" "$PATH_APP_DEL"
cp -f "$REPLACE_DIR/MainViewController.h" "$PATH_APP_DEL"
cp -f "$REPLACE_DIR/MainViewController.m" "$PATH_APP_DEL"
cp -f "$REPLACE_DIR/main.m" "$PATH_APP_DEL/../"

echo "Opening XCODE Project ..."
xed "$PATH_BUILD"