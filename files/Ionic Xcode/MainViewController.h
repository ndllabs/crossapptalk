//
//  MainViewController.h
//  MyApp
//
//  Created by Abhishek Deb (vikz91.deb@gmail.com) on 12 Aug 2019.
//

#import <Cordova/CDVViewController.h>
#import <Cordova/CDVCommandDelegateImpl.h>
#import <Cordova/CDVCommandQueue.h>
#import "AppDelegate.h"

@interface MainViewController : CDVViewController

@end

@interface MainCommandDelegate : CDVCommandDelegateImpl
@end

@interface MainCommandQueue : CDVCommandQueue
@end
