//
//  AppDelegate.h
//  MyApp
//
//  Created by Abhishek Deb (vikz91.deb@gmail.com) on 12 Aug 2019.
//

#import <Cordova/CDVViewController.h>
#import <Cordova/CDVAppDelegate.h>

#include <UnityFramework/UnityFramework.h>
#include <UnityFramework/NativeCallProxy.h>

#import "spacejoyCrossAppTalk.h"

@interface AppDelegate : CDVAppDelegate <UIApplicationDelegate, UnityFrameworkListener, NativeCallsProtocol>{}

//@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navVC;
@property(nonatomic,retain) id cspacejoyCrossAppTalk;
//@property (nonatomic, strong) MyViewController *viewController;

@property UnityFramework* ufw;
- (void)initUnity;
- (void)ShowMainView;
- (void)sendMsgToUnity:(NSString*)cmd;

@end
