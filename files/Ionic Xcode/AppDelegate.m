//
//  AppDelegate.m
//  MyApp
//
//  Created by Abhishek Deb (vikz91.deb@gmail.com) on 12 Aug 2019.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import "spacejoyCrossAppTalk.h"

#define nullptr ((void*)0)

UnityFramework* UnityFrameworkLoad()
{
    NSString* bundlePath = nil;
    bundlePath = [[NSBundle mainBundle] bundlePath];
    bundlePath = [bundlePath stringByAppendingString: @"/Frameworks/UnityFramework.framework"];
    
    NSBundle* bundle = [NSBundle bundleWithPath: bundlePath];
    if ([bundle isLoaded] == false) [bundle load];
    
    UnityFramework* ufw = [bundle.principalClass getInstance];
    if (![ufw appController])
    {
        // unity is not initialized
        [ufw setExecuteHeader: &_mh_execute_header];
    }
    return ufw;
}


@implementation AppDelegate

int gArgc = 0;
char** gArgv = nullptr;
NSDictionary* appLaunchOpts;
bool isUnityInit=false;

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    self.viewController = [[MainViewController alloc] init];
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

- (bool)unityIsInitialized { return [self ufw] && [[self ufw] appController]; }

- (void)ShowMainView
{
    [self initUnity];
    [[self ufw] showUnityWindow];
}

- (void)showHostMainWindow
{
    [self showHostMainWindow:@""];
}

- (void)showHostMainWindow:(NSString*)msg
{
    [(spacejoyCrossAppTalk*)self.cspacejoyCrossAppTalk returnResult:msg];
    [self.window makeKeyAndVisible];
}

- (void)sendMsgToUnity:(NSString*)cmd
{
    [[self ufw] sendMessageToGOWithName: "CrossAppTalk" functionName: "UnityCallbackEvent" message: [cmd UTF8String]];
}


- (void)sendMessageToIonic:(NSString*)message
{
    if( !message || [message isKindOfClass:[NSNull class]] )
        return;
    
    [(spacejoyCrossAppTalk*)self.cspacejoyCrossAppTalk receivedMessageFromUnity:message];
}

- (void)initUnity
{
    
    if(isUnityInit){
        return;
    }
    
    if([self unityIsInitialized]) {
        NSLog(@"Unity already initialized >> Unload Unity first");
        return;
    }

    
    isUnityInit=true;
    
    [self setUfw: UnityFrameworkLoad()];
    // Set UnityFramework target for Unity-iPhone/Data folder to make Data part of a UnityFramework.framework and uncomment call to setDataBundleId
    // ODR is not supported in this case, ( if you need embedded and ODR you need to copy data )
    [[self ufw] setDataBundleId: "com.unity3d.framework"];
    [[self ufw] registerFrameworkListener: self];
    [NSClassFromString(@"FrameworkLibAPI") registerAPIforNativeCalls:self];
    
    [[self ufw] runEmbeddedWithArgc: gArgc argv: gArgv appLaunchOpts: appLaunchOpts];
}

- (void)unloadButtonTouched:(UIButton *)sender
{
    if(![self unityIsInitialized]) {
        NSLog(@"Unity is not initialized >>> Initialize Unity first");
    } else {
        [UnityFrameworkLoad() unloadApplication: true];
    }
}

- (void)unityDidUnload:(NSNotification*)notification
{
    NSLog(@"unityDidUnloaded called");
    
    [[self ufw] unregisterFrameworkListener: self];
    [self setUfw: nil];
    [self showHostMainWindow:@""];
}

- (void)applicationWillResignActive:(UIApplication *)application { [[[self ufw] appController] applicationWillResignActive: application]; }
- (void)applicationDidEnterBackground:(UIApplication *)application { [[[self ufw] appController] applicationDidEnterBackground: application]; }
- (void)applicationWillEnterForeground:(UIApplication *)application { [[[self ufw] appController] applicationWillEnterForeground: application]; }
- (void)applicationDidBecomeActive:(UIApplication *)application { [[[self ufw] appController] applicationDidBecomeActive: application]; }
- (void)applicationWillTerminate:(UIApplication *)application { [[[self ufw] appController] applicationWillTerminate: application]; }


@end


int main(int argc, char* argv[])
{
    gArgc = argc;
    gArgv = argv;
    
    @autoreleasepool
    {
        if (false)
        {
            // run UnityFramework as main app
            id ufw = UnityFrameworkLoad();
            
            // Set UnityFramework target for Unity-iPhone/Data folder to make Data part of a UnityFramework.framework and call to setDataBundleId
            // ODR is not supported in this case, ( if you need embedded and ODR you need to copy data )
            [ufw setDataBundleId: "com.unity3d.framework"];
            [ufw runUIApplicationMainWithArgc: argc argv: argv];
        } else {
            // run host app first and then unity later
            UIApplicationMain(argc, argv, nil, [NSString stringWithUTF8String: "AppDelegate"]);
        }
    }
    
    return 0;
}
