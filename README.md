# Unity 2019.3.0a2+ Ionic 4 Integration
## Abhishek Deb (vikz91.deb@gmail.com)

Steps:

1. Install this cordova plugin in your ionic setup
2. Copy the `files/Angular Service/crossapptalk.service.ts` to your angular project
3. Copy `files/Unity Plugin/*.*` to your `Unity Project/Assets/Plugins`
4. Build From Ionic Cordova `ionic cordova build ios`
5. Build Unity ios
6. Open Ionic xcode workspace (`<ionic project>/platforms/ios`) and drag-n-drop  Unity xcode Project (`<unity Build Folder>`) alongside ionic project.
7. replace contents of your Ionic xode `AppDelegate` and `MainViewController` with  `files/Ionic Xcode/*` .
8. add `[ufw setDataBundleId: "com.unity3d.framework"];` to `Unity Project/MainApp/main.mm` just after `id ufw = UnityFrameworkLoad();`
9. In Unity Project right click `Data` folder and set `Target Membership` to `UnityFramework`
10. Change `Target Membership` of `Unity Project/Libraries/Plugins/ios/NativeCallProxy.h` to `UnityFramework` Public.
11. Setup Angular : use `CrossAppTalkService` to subscribe to events from unity & launch Unity with args.
12. Setup C# : use `CrossAppTalk` c# file to intialize this singleton first. subscribe to event `UnityCallbackEvent<string>`  to get params from ionic. Broadcast to `Unity2IonicMsg<string>` to send data back to Ionic. Use `OpenIonic(string)` to open Ionic.
13. In Ionic Xcode Project, remove/comment out `int main` method from `Other Sources/main.m`
14. THast all, Mate!


*In Works (WIP):*   
copy files/build.sh to your ionic project/tools/build.sh.  
Set +x permission and run it instead of ionic build.  
Do this everytime you add/remove plugins.  