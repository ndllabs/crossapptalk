/********* spacejoyCrossAppTalk.m Cordova Plugin Implementation *******/


#import "spacejoyCrossAppTalk.h"
#import "AppDelegate.h"

@implementation spacejoyCrossAppTalk

- (void)sayHello:(CDVInvokedUrlCommand*)command
{
    CDVPluginResult* pluginResult = nil;
    NSString* echo = [command.arguments objectAtIndex:0];
    if (echo != nil && [echo length] > 0) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:echo];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
    }
    
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)launchUnity:(CDVInvokedUrlCommand*)command
{
    self.cachedCallbackId = [command.callbackId copy];
    
    // [(AppDelegate *)[UIApplication sharedApplication].delegate assignIonicComms:self];
    [(AppDelegate *)[UIApplication sharedApplication].delegate ShowMainView];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.cspacejoyCrossAppTalk = self;
    NSLog(@"Launching Unity");
    if( command.arguments != nil && [command.arguments count] > 0 )
    {
        NSString* param = [command.arguments objectAtIndex:0];
        [(AppDelegate *)[UIApplication sharedApplication].delegate sendMsgToUnity:param];
    }
}

- (void)sendMessage:(CDVInvokedUrlCommand*)command
{
    NSLog(@"Calling Unity function");
    if( command.arguments != nil && [command.arguments count] > 0 )
    {
        NSString* param = [command.arguments objectAtIndex:0];
        [(AppDelegate *)[UIApplication sharedApplication].delegate sendMsgToUnity:param];
    }
}

// - (void)callBackAction:(NSString*)color {

//     CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:color];
//     [self.commandDelegate sendPluginResult:result callbackId:self.cachedCallbackId];
// }


- (void)receivedMessageFromUnity:(NSString*)message
{
    NSString *s = @"Msg Rcvd from Unity : ";
    s = [s stringByAppendingString:message];
    NSLog(@"%@", s);
    
    CDVPluginResult* cdvResult = [CDVPluginResult
                                  resultWithStatus:CDVCommandStatus_OK
                                  messageAsString:[NSString stringWithFormat:@"%@%@",@"MESSAGE",message]];
    
    [cdvResult setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:cdvResult callbackId:self.cachedCallbackId];
}

- (void)returnResult:(NSString*)result
{
    NSString *s = @"Result returned: ";
    s = [s stringByAppendingString:result];
    NSLog(@"%@", s);
    
    CDVPluginResult* cdvResult = [CDVPluginResult
                                  resultWithStatus:CDVCommandStatus_OK
                                  messageAsString:[NSString stringWithFormat:@"%@%@",@"RETURN",result]];
    
    [cdvResult setKeepCallbackAsBool:NO];
    [self.commandDelegate sendPluginResult:cdvResult callbackId:self.cachedCallbackId];
}

@end
