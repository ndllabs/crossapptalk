//
//  spacejoyCrossAppTalk.h
//  MyApp
//
//  Created by Abhishek Deb on 13/08/19.
//
#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>
#import "AppDelegate.h"

NS_ASSUME_NONNULL_BEGIN

@interface spacejoyCrossAppTalk : CDVPlugin

@property (readwrite, strong) NSString* cachedCallbackId;
- (void)sayHello:(CDVInvokedUrlCommand*)command;
- (void) launchUnity:(CDVInvokedUrlCommand*)command;
- (void) sendMessage:(CDVInvokedUrlCommand*)command;
- (void) receivedMessageFromUnity:(NSString*)message;
- (void) returnResult:(NSString*)result;

@end

NS_ASSUME_NONNULL_END
